#include <hdf5.h>

int main(int argc, char *argv[]){

    unsigned majnum;
    unsigned minnum;
    unsigned relnum;

    H5get_libversion( &majnum, &minnum, &relnum );

    printf("HDF5 version:\t%d.%d.%d\n", majnum, minnum, relnum);

}

        
